﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolidClassDemo
{
    internal class Equipment
    {
        public string Description { get; set; }
        public double Price { get; set; }
        public Equipment(string description, double price)
        {
            Description = description;
            Price = price;
        }
    }
}

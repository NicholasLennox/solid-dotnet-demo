﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace SolidClassDemo
{
    internal class ElectronicsInvoiceLine
    {
        public Equipment Equipment { get; set; }
        public int Quantity { get; set; }
        public double Discount { get; set; } = 1;

        public ElectronicsInvoiceLine(Equipment equipment, int quantity, double discount)
        {
            Equipment = equipment;
            Quantity = quantity;
            Discount = discount;
        }

        private double GetLineTotal()
        {
            return Equipment.Price * Quantity * Discount;
        }

        private string GetDiscountText()
        {      
            if (Discount == 0.95)
                return "Pensioner";
            if (Discount == 0.9)
                return "Student";
            return "Custom";
        }

        public void PrintInvoiceLine()
        {
            StringBuilder line = new StringBuilder();
            line.Append("Description: " + Equipment.Description);
            line.Append(", Price: " + Equipment.Price);
            line.Append(", Quantity: " + Quantity);
            line.Append(", Discount applied: " + Discount);
            line.Append(", Discount type: " + GetDiscountText());
            line.Append(", Total: " + GetLineTotal());

            Console.WriteLine(line.ToString());
        }

        public void SaveToFile()
        {
            // Build string
            StringBuilder line = new StringBuilder();
            line.Append("Description: " + Equipment.Description);
            line.Append(", Price: " + Equipment.Price);
            line.Append(", Quantity: " + Quantity);
            line.Append(", Discount applied: " + Discount);
            line.Append(", Discount type: " + GetDiscountText());
            line.Append(", Total: " + GetLineTotal());
            // Save to text file - don't worry about this yet
            string pathToFile = "output.txt";
            try
            {
                File.WriteAllTextAsync(pathToFile, line.ToString());
            } catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

    }
}

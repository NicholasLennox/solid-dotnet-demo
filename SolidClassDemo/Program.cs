﻿namespace SolidClassDemo
{
    internal class Program
    {
        static void Main(string[] args)
        {
            ElectronicsInvoiceLine invoice = new
                ElectronicsInvoiceLine(new Equipment("Cannon Ultra-print", 100), 2, 0.9);
            invoice.PrintInvoiceLine();
            invoice.SaveToFile();

        }
    }
}